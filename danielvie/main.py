#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os
import webapp2
import jinja2
from urllib import urlopen


from google.appengine.ext import db

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir), autoescape=True)

class ultimos_sites(db.Model):
	message = db.StringProperty(
		required=True)
	when = db.DateTimeProperty(
		auto_now_add=True)

class Handler(webapp2.RequestHandler):
	def write(self, *a, **kw):
		self.response.out.write(*a, **kw)

	def render_str(self, template, **params):
		t = jinja_env.get_template(template)
		return t.render(params)

	def render(self, template, **kw):
		self.write(self.render_str(template, **kw))

import logging
from google.appengine.api import memcache



class MainHandler(Handler):
	def get(self):
		self.render('calculoLancadoras.html')

class Manga(Handler):
	def get(self):
		delete = self.request.get('delete')
		# check_deletar = self.request.get('check_deletar', allow_multiple=True)
		# check_deletar = ''


		if delete:
			logging.info('deleta_get')
			q = db.GqlQuery(
				"SELECT * FROM ultimos_sites where __key__ = KEY('ultimos_sites', %s)"%delete)
			results = q[0]
			results.delete()
			self.redirect('/manga')
		else:
			q = [];

		sites = db.GqlQuery(
			'SELECT * FROM ultimos_sites ORDER BY message DESC')
		self.render('manga.html',sites=sites,q=q)

	def post(self):
		# lendo campos POST
		img_adr = self.request.get('img_adr')
		prefixo = self.request.get('prefixo')
		sufixo  = self.request.get('sufixo')
		zero10  = self.request.get('zero10')

 		# ajustando zero10
		if zero10 == 'on':
			zero10 = 'checked'

		# lendo DB com ultimos sites
		sites   = ultimos_sites(message=img_adr)
		sites.put()

		# renderizando pagina
		self.render('manga.html',img_adr=img_adr, prefixo=prefixo, sufixo=sufixo, zero10=zero10)

class Acoes(Handler):
	def get(self):
		self.render('acoes.html');

class Acoes2(Handler):
	def get(self):
		self.render('acoes2.html');
	def post(self):
		self.write('formulario submetido no modo POST');
		
class Docs(Handler):
	def get(self):
		self.render('docs.html');

class Acoes_medium(Handler):
	def get(self):
		acao = self.request.get('acao');
		url = "http://finance.google.com/finance/info?client=ig&q=BVMF:%s"%(acao);
		html = urlopen(url).read();
		self.render('acoes_medium.html',acao=html);

app = webapp2.WSGIApplication([('/', MainHandler),
							   ('/manga', Manga),						   
							   ('/acoes', Acoes),
							   ('/acoes2', Acoes2),
							   ('/acoes_medium', Acoes_medium),
							   ('/docs', Docs)

							  ], debug=True)