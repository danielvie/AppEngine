contador = 1;
fimContador = 30;

var target;
var evento;
function vai(contador_vai) {
	// recebendo valor da pagina
	html = document.getElementById('pagina').value;
	
	if (html.search(/^http/g) == -1)
		html = 'http://'+html;

	base = html.replace(/[/]\d+\.(jpg|png|gif)/i,"");
	
	nro  = parseInt(base.replace(/.+\//i,""));
	ext  = html.replace(/.+(?=(jpg|gif|png))/i,"");

	// inserindo link prox edicao
	nro_prox  = nro+1;
	base_prox = base.replace(/\d+$/i,nro_prox);
	html_prox = base_prox + "/" + "1." + ext;

	prox = document.getElementById('prox-edicao');
	prox.innerHTML = '<a href="#" class="badge badge-prox" onclick="return vai_site(\'' + html_prox + '\')">'+(nro+1)+'</a>';


	// deletando link maisPaginas
	var maisPaginas = document.getElementById('maisPaginas');
	if (maisPaginas != null) maisPaginas.remove();

	// inserindo figuras
	contador = repete(base,ext,contador_vai)
	
	return false;
}
function vai_site(site) {
	document.getElementById('pagina').value = site;
	vai(1);
	return false;
}

function repete(base,ext,contador) {
	cont           = document.getElementById('conteudo');
	if (contador == 1) cont.innerHTML = "";
	for (var i = contador; i <= contador + fimContador; i++) {

		// lendo valores
		zero10  = document.getElementById('zero10').checked;
		nro     = (i < 10 && zero10)? '0'+i:i;
		pag     = 1;

		// objeto da imagem
		im      = document.createElement('img');

		prefixo = document.getElementById('prefixo').value;
		sufixo  = document.getElementById('sufixo').value;

		imagem  = base + '/' + prefixo + nro + sufixo + '.' + ext;

		im.setAttribute('src',imagem);
		im.setAttribute('onclick','return clicaImagem(event);')

		var d = document.createElement('div');
		d.setAttribute('class','cont-img')

		// objeto das labels com nro da imagem
		lini = document.createElement('label');
		lini.setAttribute('class','nro-imagem nro-imagem-ini badge');
		lini.innerHTML = nro;
		lfim = document.createElement('label');
		lfim.setAttribute('class','nro-imagem nro-imagem-fim badge');
		lfim.innerHTML = nro;

		// inserindo labels e imagem na div
		d.appendChild(lini)
		d.appendChild(im);
		d.appendChild(lfim)
		
		// inserindo div na div de conteudo
		cont.appendChild(d);
	}

	var e       = document.createElement('div');
	e.setAttribute('id','maisPaginas')
	e.innerHTML = "<a href='#' onclick='return vai(contador);'>mais paginas</a>";
	cont.appendChild(e);
	return i;
}

function criaElemento(endereco){
	// deletar elemento que ja existe
	existe = document.getElementById('opcoes_imagem');
	if (existe != null) existe.remove();

	// criar div principal
	d = document.createElement('div');
	d.setAttribute('id','opcoes_imagem');
	d.setAttribute('class','div_opcoes_column');
	d.style.left  = evento.target.x+'px'; 
	d.style.top   = evento.target.y+'px';
	d.style.width = evento.target.clientWidth+'px';

	// criar elementos
	// link
	link     = document.createElement('p');
	link.setAttribute('class','srcImagem');
	linkTxt  = document.createTextNode(endereco);
	link.appendChild(linkTxt);
	
	// inserir elementos na div
	d.appendChild(link);

	return d;
}

function clicaImagem(e){
	mudar_x();
	evento   = e;
	target   = e.target;
	endereco = target.getAttribute('src');
	d        = criaElemento(endereco);

	// target.style.opacity = 0.4;
	target.parentElement.appendChild(d);
	return false;
}

function mudar_pre() {
	alert('entrei mudar_pre');
	mudar_x();
	return false;
}

function mudar_pos() {
	alert('entrei mudar_pos');
	mudar_x();
	return false;
}

function mudar_ext() {
	alert('entrei mudar_ext');
	mudar_x();
	return false;
}

function mudar_edit() {
	// cria elemento form
	form = document.createElement('form');

	// cria elemento input
	ei = document.createElement('input');
	ei.setAttribute('type','text');
	ei.setAttribute('size','100px');
	ei.setAttribute('id','novo_src');
	ei.setAttribute('value',target.getAttribute('src'))

	// cria elemento submit
	es = document.createElement('input');
	es.setAttribute('type','submit');
	es.setAttribute('value','muda');
	es.setAttribute('onclick','return mudar_aplicar_mudanca();')
	es.style.width = '4em';

	// inserir na div de edicao
	form.appendChild(ei);
	form.appendChild(es);

	div = document.getElementById('opcoes_imagem');
	div.innerHTML = '';
	div.appendChild(form);

	// retira atributo class:div_opcoes_column
	div.removeAttribute('class','div_opcoes_column');
	return false;
}

function mudar_aplicar_mudanca () {
	// recebendo alteracoes
	src = document.getElementById('novo_src').value;
	
	// aplicando alteracoes
	target.setAttribute('src',src);

	// encerrando camada
	mudar_x();
	return false;
}

function mudar_del_el() {
	target.parentElement.remove();
	mudar_x();
}

function mudar_sair () {
	mudar_x();
	return false;
}
function mudar_x() {
	var el = document.getElementById('opcoes_imagem');
	if (el != null) {
		el.remove();
		target.style.opacity = 1;
	}
}	

function submit_pagina() {
		form = document.getElementById("formulario");
		form.submit();
	}	