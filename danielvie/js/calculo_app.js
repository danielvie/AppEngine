window.onload = function() {
    // Display the todo items.
    contaDB.open(refreshContas);

    // Get references to the form elements.
    var contaForm = document.getElementById('formLMU');
    var newTodoInput = document.getElementById('new-todo');

    // Handle new todo item form submissions.
    contaForm.onsubmit = function() {
        console.log('formulario submetido!');
        
        salva_conta();

        // Reset the input field.
        // newTodoInput.value = '';

        // Don't send the form.
        return false;
    };
};

function salva_conta() {

    // lendo valores dos campos do formulario
    var titulo      = document.getElementById('titulo').value;
    var lmu_E       = document.getElementById('lmu_E').value;
    var lmu_N       = document.getElementById('lmu_N').value;
    var ref_E       = document.getElementById('ref_E').value;
    var ref_N       = document.getElementById('ref_N').value;
    var targ_E      = document.getElementById('targ_E').value;
    var targ_N      = document.getElementById('targ_N').value;
    var AO          = document.getElementById('AO').value;
    var lmu_heading = document.getElementById('lmu_heading').value;

    // Tratar dados aqui

    // Montando estrutura de dados
    conta = {'titulo':titulo, 'lmu_E':lmu_E, 'lmu_N':lmu_N, 'ref_E':ref_E, 'ref_N':ref_N, 'targ_E':targ_E, 'targ_N':targ_N, 'AO':AO, 'lmu_heading':lmu_heading};

    // Create the todo item.
    contaDB.createConta(conta, function(todo) {
        refreshContas();
    });
}

function carregar_conta(id) {

    contaDB.fetchConta(id,function (conta) {
        document.getElementById('titulo').value      = conta.titulo;
        document.getElementById('lmu_E').value       = conta.lmu_E;
        document.getElementById('lmu_N').value       = conta.lmu_N;
        document.getElementById('ref_E').value       = conta.ref_E;
        document.getElementById('ref_N').value       = conta.ref_N;
        document.getElementById('targ_E').value      = conta.targ_E;
        document.getElementById('targ_N').value      = conta.targ_N;
        document.getElementById('AO').value          = conta.AO;
        document.getElementById('lmu_heading').value = conta.lmu_heading;
        return false;
    })
}

// Update the list of todo items.
function refreshContas() {  
    contaDB.fetchContas(function(contas) {
        var contaList = document.getElementById('conta-do-ze-memoria');
        contaList.innerHTML = '';

        for(var i = 0; i < contas.length; i++) {
            // Read the todo items backwards (most recent first).
            index = (contas.length - 1 - i);
            var conta = contas[(contas.length - 1 - i)];

            // span com titulo
            txt = conta.formulario.titulo;
            var spa = document.createElement('span');
            spa.innerHTML = txt;

            // link deletar
            var del = document.createElement('a');
            del.setAttribute('href','#');
            del.setAttribute('data-id',conta.timestamp);
            del.setAttribute('class','btn btn-xs btn-danger btn_icon');
            del.setAttribute('onClick','return botao_deleta_conta(this);')
            span = document.createElement('span');
            span.setAttribute('class','glyphicon glyphicon-floppy-remove btn_deletar_conta');
            del.appendChild(span);


            // link carregar
            var car = document.createElement('a');
            car.setAttribute('href','#');
            car.setAttribute('data-id',conta.timestamp);
            car.setAttribute('class','btn btn-xs btn-primary btn_icon');
            car.setAttribute('onClick','return botao_carrega_conta(this);')
            span = document.createElement('span');
            span.setAttribute('class','glyphicon glyphicon-floppy-open btn_deletar_carrega');
            car.appendChild(span);
            
            
            // colocar link del / carregar
            var div = document.createElement('div');
            div.className = 'carregar-deletar-elemento';
            div.appendChild(del);
            div.appendChild(car);

            // inserindo elementos na lista
            var pai = document.createElement('div');
            pai.className = 'conta-do-ze';

            pai.appendChild(spa);
            pai.appendChild(div);
            contaList.appendChild(pai);
        }
    });
}

function carrega_db() {
    mem = document.getElementById('memoria-btn');
    lis = document.getElementById('conta-do-ze-memoria');

    cla = lis.getAttribute('class');
    if (cla == "lista") {
        cla = "btn btn-primary btn-md";
    } else {
        cla = "btn btn-default btn-md";
    }
    mem.setAttribute('class',cla);

    toogle_lista_conta_do_ze();
    return false;
}

function toogle_lista_conta_do_ze() {
    lista = document.getElementById('conta-do-ze-memoria');
    if (lista.getAttribute('class') == "lista"){
        lista.setAttribute('class','lista abre-lista-contas');
    } else {
        lista.setAttribute('class','lista');
    }
}

function submit_conta_ze() {
    return false;
}       

function salva_db() {
    submit_conta_ze();
    salva_conta();
    return false;
}

function limpa_conta_do_ze() {
    document.getElementById("formLMU").reset();
}

function botao_deleta_conta(el) {
    var id = parseInt(el.getAttribute('data-id'));
    txt = 'deletar ' + id;
    contaDB.deleteConta(id, refreshContas);
    return false;
}

function botao_carrega_conta(el) {
    var id = parseInt(el.getAttribute('data-id'));
    txt = 'carregar ' + id;
    carregar_conta(id);
    return false;
}