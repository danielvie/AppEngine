/**
 * Criando indexedDB
 */
var contaDB = (function() {
 	var cDB = {};
 	var datastore = null;

	// TODO: Add methods for interacting with the database here.
	/**
	* Open a connection to the datastore.
	*/
	cDB.open = function(callback) {
		// Database version.
		var version = 2;

		// Open a connection to the datastore.
		var request = indexedDB.open('conta_do_ze', version);

		// Handle datastore upgrades.
		request.onupgradeneeded = function(e) {
			var db = e.target.result;

			e.target.transaction.onerror = cDB.onerror;

			// Delete the old datastore.
			if (db.objectStoreNames.contains('conta')) {
				db.deleteObjectStore('conta');
			}

			// Create a new datastore.
			var store = db.createObjectStore('conta', {
				keyPath: 'timestamp'
			});
		};

		// Handle successful datastore access.
		request.onsuccess = function(e) {
			// Get a reference to the DB.
			datastore = e.target.result;
			console.log('db criado com sucesso.');

			// Execute the callback.
			callback();
		};

		// Handle errors when opening the datastore.
		request.onerror = cDB.onerror;
	};

	/**
	 * Fetch all of the todo items in the datastore.
	 */
	cDB.fetchConta = function(id,callback) {
	 	var db = datastore;
	 	var transaction = db.transaction(['conta'], 'readonly');
	 	var objStore = transaction.objectStore('conta');

	 	var keyRange = IDBKeyRange.lowerBound(0);
	 	var request = objStore.get(id);
	 	// var request = db.transaction(["estudantes"],"readwrite").objectStore("estudantes").get(codigo);

	 	var conta = [];

	 	transaction.oncomplete = function(e) {
		    // Execute the callback function.
		    callback(conta);
		};

		request.onsuccess = function(e) {
			// request.result.nome
			var result = e.target.result;

			if (!!result == false) {
				return;
			}

		    conta = e.target.result.formulario;
		};

		request.onerror = cDB.onerror;
	};

	/**
	 * Fetch all of the todo items in the datastore.
	 */
	cDB.fetchContas = function(callback) {
	 	var db = datastore;
	 	var transaction = db.transaction(['conta'], 'readonly');
	 	var objStore = transaction.objectStore('conta');

	 	var keyRange = IDBKeyRange.lowerBound(0);
	 	var cursorRequest = objStore.openCursor(keyRange);

	 	var contas = [];

	 	transaction.oncomplete = function(e) {
		    // Execute the callback function.
		    callback(contas);
		};

		cursorRequest.onsuccess = function(e) {
			var result = e.target.result;

			if (!!result == false) {
				return;
			}

			contas.push(result.value);

			result.continue();
		};

		cursorRequest.onerror = cDB.onerror;
	};

	/**
	 * Create a new todo item.
	 */
	cDB.createConta = function(dados, callback) {
		// Get a reference to the db.
		var db = datastore;

		// Initiate a new transaction.
		var transaction = db.transaction(['conta'], 'readwrite');

		// Get the datastore.
		var objStore = transaction.objectStore('conta');

		// Create a timestamp for the todo item.
		var timestamp = new Date().getTime();

		// Create an object for the todo item.
		var conta = {
			'formulario': dados,
			'timestamp': timestamp
		};

		// Create the datastore request.
		var request = objStore.put(conta);

		// Handle a successful datastore put.
		request.onsuccess = function(e) {
			// Execute the callback function.
			console.log('conta salva.');
			callback(conta);
		};

		// Handle errors.
		request.onerror = function(e) {
			console.log(cDB.onerror);
		}
	};

	/**
	* Delete a todo item.
	*/
	cDB.deleteConta = function(id, callback) {
		var db = datastore;
		var transaction = db.transaction(['conta'], 'readwrite');
		var objStore = transaction.objectStore('conta');

		var request = objStore.delete(id);

		request.onsuccess = function(e) {
			callback();
			console.log('conta deletada.');
		};

		request.onerror = function(e) {
			console.log(e);
		};
	};

	// Export the cDB object.
	return cDB;
}());