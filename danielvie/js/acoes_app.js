url = "http://query.yahooapis.com/v1/public/yql?q=select symbol,LastTradePriceOnly,Change from yahoo.finance.quote where symbol in ('$acao$')&format=json&env=store://datatables.org/alltableswithkeys&callback=";
// url_google = "acoes_medium?acao=$acao$";
url_google = "http://query.yahooapis.com/v1/public/yql?q=select symbol,LastTradePriceOnly,Change from yahoo.finance.quote where symbol in ('$acao$')&format=json&env=store://datatables.org/alltableswithkeys&callback=";

window.onload = function() {
    // Display the todo items.
    acaoDB.open(criaAcoes);
    
    // Get references to the form elements.
    // var acaoForm = document.getElementById('formLMU');
    // var newTodoInput = document.getElementById('new-todo');

    // Handle novas acoes
    form = document.getElementById('formulario');
    form.addEventListener('submit',function(evt){
        evt.preventDefault(); // evitar que formulario seja enviado
        salvaAcao();
        // escreveResposta();
    })

    go = document.getElementById('formulario-go');
    go.addEventListener('click',function(evt){
        salvaAcao();
        // escreveResposta();
    })

    muda = document.getElementById('formulario-muda');
    muda.addEventListener('click',function(evt){
        mudaAcao();
        // escreveResposta();
    })
};

function mudaAcao() {
    console.log('mudaAcao');

    form_nome  = document.getElementById('nome-acao').value;
    acoes = document.getElementsByClassName('acao-elemento');

    for(var i = 0; i < acoes.length; i++) {
        aux = acoes[i].getElementsByClassName('span-nome')[0].innerHTML;
        if (aux.localeCompare(form_nome) == 0) {
            id = acoes[i].getAttribute('data-id');
            console.log('mudaAcao: ' + aux + ' id: ' + id);
        }
    }

    return false;
}

function preencheForm(el) {
    console.log('preencheForm');

    div = el.parentElement.parentElement;
    form_nome  = document.getElementById('nome-acao');
    form_qnt   = document.getElementById('qnt-acoes');
    form_valor = document.getElementById('valor-inicial');

    form_nome.value  = el.innerHTML;
    form_qnt.value   = div.getElementsByClassName('span-qnt')[0].innerHTML;
    form_valor.value = div.getElementsByClassName('span-valor-atual')[0].innerHTML;
    return false;
}

function salvaAcao() {

    // lendo valores dos campos do formulario
    var nome_acao     = document.getElementById('nome-acao').value.toUpperCase();
    var qnt           = document.getElementById('qnt-acoes').value;
    var valor_inicial = document.getElementById('valor-inicial').value;
    
    // pegando valores atualizados da acao
    getJSON(url.replace("$acao$",nome_acao+'.SA'),function(err, data){
        res = data.query.results.quote;

        console.log(url.replace("$acao$",nome_acao+'.SA'));

        valor_atual = parseFloat(res.LastTradePriceOnly).toFixed(2);
        change      = res.Change;

        // Montando estrutura de dados
        conta = {'nome':nome_acao, 'qnt':qnt, 'valor_inicial':valor_inicial, 'valor_atual':valor_atual, 'change':change, 'fechada':0};

        console.log(conta);
        // Create the todo item.
        acaoDB.createAcao(conta, function(todo) {
            criaAcoes();
        });
    });
}

// Update the list of todo items.
function criaAcoes() {
    console.log('criaAcoes') 
    acaoDB.fetchAcoes(function(contas) {
        var acaoLista = document.getElementById('acao-lista');
        acaoLista.innerHTML = '<h3>acoes abertas</h3>';
        
        var acaoListaFechada = document.getElementById('acao-lista-fechada');
        acaoListaFechada.innerHTML = '<h3>acoes fechadas</h3>';

        for(var i = 0; i < contas.length; i++) {
            // Read the todo items backwards (most recent first).
            index = (contas.length - 1 - i);
            var conta = contas[(contas.length - 1 - i)];


            qnt           = parseInt(conta.dados.qnt);
            valor_inicial = parseFloat(conta.dados.valor_inicial)
            valor_atual   = parseFloat(conta.dados.valor_atual)
            change        = parseFloat(conta.dados.change)

            // calculando lucro
            var lucro    = (valor_atual - valor_inicial) * parseFloat(qnt);
            var perc     = (valor_atual / valor_inicial - 1) * 100;
            var perc_dia = (valor_atual / (valor_atual - change) - 1) * 100;

            // escolhendo tipo de label (lucro ou prejuiso)
            if (lucro > 0){
                label = 'label-success';
            }
            else{
                label = 'label-danger';
            }

            // ajustando formato para mostrar na pagina
            negativo = (lucro < 0);
            lucro    = 'R$' + Math.abs(lucro.toFixed(2));
            perc     = Math.abs(perc.toFixed(2)) + '%';
            perc_dia = perc_dia.toFixed(2) + '%';
                        
            // cria lista de acoes abertas
            if (conta.dados.fechada == 0){
                html  = '<div class="acao-elemento img-rounded" data-id="' + conta.timestamp +'">';
                html += '    <div class="acao-elemento-barra-lat">';
                html += '    </div>';
                html += '    <div class="acao-col">';
                html += '        <span class="span-lista-acao span-nome label ' + label +'" onClick="return preencheForm(this);">' + conta.dados.nome +'</span>';
                html += '        <span class="span-lista-acao span-lucro ' + ((negativo)?'span-lucro-negativo':'') + '">' + lucro + ' (' + perc + ')' + '</span>';
                html += '    </div>';
                html += '    <div class="acao-col">';
                html += '        <span class="span-lista-acao span-qnt">' + conta.dados.qnt + '</span>';
                html += '        <span class="span-lista-acao span-valor-inicial">' + conta.dados.valor_inicial + '</span>';
                html += '        <span class="span-lista-acao span-valor-atual">' + conta.dados.valor_atual + '</span>';
                html += '    </div>';
                html += '    <div class="acao-col">';
                html += '        <span class="span-lista-acao span-change">' + conta.dados.change + ' (' + perc_dia + ')' + '</span>';
                html += '    </div>';
                html += '    <div class="acao-botoes">';
                html += '        <a href="#" data-id="' + conta.timestamp +'" class="btn btn-xs btn-success btn_icon" onclick="return botaoFechaAcao(this);">';
                html += '            <span class="glyphicon glyphicon-ok acao-btn"></span>';
                html += '        </a>';
                html += '        <a href="#" data-id="' + conta.timestamp +'" class="btn btn-xs btn-primary btn_icon btn_atualiza_valor" onclick="return botaoAtualizaValorAtual(this);">';
                html += '            <span class="glyphicon glyphicon-refresh acao-btn"></span>';
                html += '        </a>';
                html += '    </div>';
                html += '    <div class="acao-fechamento img-rounded acao-fechamento-invisivel">';
                html += '        <form class="form-fechamento" action="void();" onSubmit="return formFechamentoSubmit(this);" method="post" id="formulario_' + i + '">';
                html += '            <div class="form-group text-center">';
                html += '                <div class="col-xs-10">';
                html += '                    <div class="input-group">';
                html += '                        <span class="input-group-addon" onclick="return focaSpan(this);">valor:</span>';
                html += '                        <input class="form-control acao-valor-fechamento" placeholder="valor" aria-describedby="addon_acao" type="text" value=""/>';
                html += '                        <span class="input-group-btn">';
                html += '                            <button class="btn btn-success" type="button" onClick="return submitFechamento(this);">Go!</button>';
                html += '                        </span>';
                html += '                    </div>';
                html += '                </div>';
                html += '            </div>';
                html += '            <input type="submit" style="visibility: hidden;"/>';
                html += '        </form>';
                html += '        <div class="acao-botoes">';
                html += '            <a href="#" data-id="' + conta.timestamp +'" class="btn btn-xs btn-danger btn_icon" onclick="return fechaJanelaFechamento(this);">';
                html += '                <span class="glyphicon glyphicon-remove acao-btn"></span>';
                html += '            </a>';
                html += '        </div>';
                html += '    </div>';
                html += '</div>';
                acaoLista.innerHTML += html;
            } else { // cria lista de acoes fechadas
                html  = '<div class="acao-elemento img-rounded" data-id="' + conta.timestamp +'">';
                html += '    <div class="acao-col">';
                html += '        <span class="span-lista-acao span-nome label ' + label +'">' + conta.dados.nome +'</span>';
                html += '        <span class="span-lista-acao span-lucro">' + lucro + ' (' + perc + ')' + '</span>';
                html += '    </div>';
                html += '    <div class="acao-col">';
                html += '        <span class="span-lista-acao span-qnt">' + conta.dados.qnt + '</span>';
                html += '        <span class="span-lista-acao span-valor-inicial">' + conta.dados.valor_inicial + '</span>';
                html += '        <span class="span-lista-acao span-valor-atual">' + conta.dados.valor_atual + '</span>';
                html += '    </div>';
                html += '    <div class="acao-botoes">';
                html += '        <a href="#" data-id="' + conta.timestamp +'" class="btn btn-xs btn-danger btn_icon" onclick="return botaoDeletaAcao(this);">';
                html += '            <span class="glyphicon glyphicon-remove acao-btn"></span>';
                html += '        </a>';
                html += '        <a href="#" data-id="' + conta.timestamp +'" class="btn btn-xs btn-primary btn_icon" onclick="return botaoAbreAcao(this);">';
                html += '            <span class="glyphicon glyphicon-share-alt acao-btn flip-horizontal"></span>';
                html += '        </a>';
                html += '    </div>';
                html += '</div>';
                acaoListaFechada.innerHTML += html;
            }
        }
    });
}


// funcao para pegar JSON
var getJSON = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
        var status = xhr.status;
        if (status == 200) {
            callback(null, xhr.response);
        } else {
            callback(status);
        }
    };
    xhr.send();
};

// funcao para pegar JSON
var getJSON_txt = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'text';
    xhr.onload = function() {
        var status = xhr.status;
        if (status == 200) {
            callback(null, xhr.response);
        } else {
            callback(status);
        }
    };
    xhr.send();
};

function escreveResposta() {
    // lendo valor do form
    acao = document.getElementById('nome-acao').value;
    
    // apontando para div de resposta
    valor_atual = document.getElementById('span-valor-atual');

    // fazendo requisicao json
    url = "http://query.yahooapis.com/v1/public/yql?q=select symbol,LastTradePriceOnly,Change from yahoo.finance.quote where symbol in ('$acao$')&format=json&env=store://datatables.org/alltableswithkeys&callback=";
    // url = "http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.quote where symbol in ('$acao$')&format=json&env=store://datatables.org/alltableswithkeys&callback=";
    console.log(url.replace("$acao$",acao+'.SA'));
    getJSON(url.replace("$acao$",acao+'.SA'),function(err, data){
        // keys = Object.keys(data.query.results.quote);
        res = data.query.results.quote;

        // montando txt de resposta
        // txt = 'acao: ' + res.symbol.replace('.SA','').toUpperCase() + ': ' + res.LastTradePriceOnly;

        // escrevendo resposta
        valor_atual.innerHTML = res.LastTradePriceOnly.toFixed(2);
        return false;
    })

}

function botaoDeletaAcao(el) {
    // lendo id da acao de fechamento
    div = el;
    while(div.classList[0] != "acao-elemento"){
        div = div.parentElement;
    }
    
    id = parseInt(div.getAttribute('data-id'));

    acaoDB.deleteAcao(id,function(){
        criaAcoes();
    });
    
    return false;
}

function botaoAbreAcao(el) {
    // lendo id da acao de fechamento
    div = el;
    while(div.classList[0] != "acao-elemento"){
        div = div.parentElement;
    }
    
    id = parseInt(div.getAttribute('data-id'));

    acaoDB.abreAcao(id, function(){
        console.log('abri acao ' + id);
        criaAcoes();
    });

    return false;
}

function botaoFechaAcao(el) {
    abreJanelaFechamento(el);
    return false;
}

var a;
var b;
var c;
var ini;
var fim;

function testaUrl(url) {
    // console.log(url);
    getJSON_txt(url, function(err, data){
        // console.log(err);
        // console.log(data);
        txt = data.split('\n');

        ini = 0;
        fim = txt.length;
        for (var i = 0; i < txt.length; i++) {
            if (txt[i].localeCompare("{") == 0){
                ini = i;
            }
            if (txt[i].localeCompare("}") == 0){
                fim = i;
            }
        }

        // testaUrl("http://finance.google.com/finance/info?client=ig&q=BVMF:PETR4")

        txt.splice(fim+1,txt.length - fim - 1);
        txt.splice(0,ini);
        b = txt;
        c = b.join('');
        a = data;

        console.log(JSON.parse(c));
    })
}

function botaoAtualizaValorTodos() {
    console.log("botaoAtualizaValorTodos");
    
    // encontrando links de refresh das acoes
    els = document.getElementsByClassName("btn_atualiza_valor");
    
    // executando
    execBotaoAtualizaValorTodos(els,0);
    
    return false;

}

function execBotaoAtualizaValorTodos(els,cont) {

    els[cont].click();

    if ((cont + 1) < els.length) {
        setTimeout(function(){
            execBotaoAtualizaValorTodos(els,cont + 1)
        }, 1500);
    } else {
        return true;
    }
}


function botaoAtualizaValorAtual(el) {
    // var id = parseInt(el.getAttribute('data-id'));
    div = el.parentElement.parentElement;
    var nome = div.getElementsByClassName('span-nome')[0].innerHTML;
    var spa_valor_atual   = div.getElementsByClassName('span-valor-atual')[0];
    var spa_valor_inicial = div.getElementsByClassName('span-valor-inicial')[0];
    var spa_qnt           = div.getElementsByClassName('span-qnt')[0];
    var spa_lucro         = div.getElementsByClassName('span-lucro')[0];
    var spa_perc          = div.getElementsByClassName('span-perc')[0];
    var spa_change        = div.getElementsByClassName('span-change')[0];

    url_ = url_google;
    var json_url = url_.replace("$acao$",nome+'.SA');
    console.log(json_url);

    getJSON(json_url, function(err, data){
        
        // lendo pagina
        // txt = data.split('\n').join('');

        // substituindo &#34; -> "
        // txt = txt.replace(/\&#34;/g,'"');

        // tirando termos antes de '{'
        // txt = txt.replace(/^.*?(?={)/g,'');
        
        // tirando termos depois de '}'
        // txt = txt.replace(/}.*/g,'}');
        
        // res = JSON.parse(txt);
        res = data.query.results.quote;
        // escrevendo resposta
        valor  = res.LastTradePriceOnly;
        // valor  = "17.00";
        change = res.Change;
        
        // atualizando banco de dados
        id = parseInt(div.getAttribute('data-id'));
        acaoDB.updateAcao(id,valor,change,function(e){
            
            // encontrando elementos na lista
            lista  = document.getElementsByClassName('acao-elemento');
            div_id = div.getAttribute('data-id');
            for (var i = 0; i < lista.length; i++) {
                data_id = lista[i].getAttribute('data-id');
                if (data_id.localeCompare(div_id) == 0){
                    aux = lista[i].getElementsByClassName('acao-elemento-barra-lat')[0];
                }
            }
            
            // mudando nome classe do elemento
            aux.className = 'acao-elemento-barra-lat acao-elemento-barra-lat-ok';
            setTimeout(function(){
                aux.className = 'acao-elemento-barra-lat';
                criaAcoes();
            }, 500);

        })

        return false;
    })

    return false;
}

function abreJanelaFechamento(el) {

    var id = parseInt(el.getAttribute('data-id'));
    div_fecha = el.parentElement.parentElement.getElementsByClassName('acao-fechamento')[0];
    div_fecha.className = 'acao-fechamento img-rounded';

    // encontrando valor atual para alimentar input
    pai = div_fecha.parentElement;
    valor_atual = pai.getElementsByClassName('span-valor-atual')[0].innerHTML;
    
    input = div_fecha.getElementsByClassName('acao-valor-fechamento')[0];
    input.value = valor_atual;
    
    input.focus();
    input.select();
    return false;
}

function fechaJanelaFechamento(el) {
    // apontando para div 'acao-fechamento' e escondendo ela
    div           = el.parentElement.parentElement;
    div.className = 'acao-fechamento acao-fechamento-invisivel';

    // lendo valores nos campos da acao que esta sendo fechada
    res           = getDados(el);

    // lendo valor do input
    valor = div.getElementsByClassName('acao-valor-fechamento')[0].value;

    // acaoDB.fechaAcao(id,valor,function(e){
    //     console.log('acao fechada!');
    // });
    // console.log('atualizar valor atual da ' + res.nome + ', de ' + res.valor_atual + ' para ' + res.)

    return false;
}

function focaSpan(el) {
    input = el.parentElement.getElementsByTagName('input')[0];
    input.focus();
    input.select();
    return false;
}

function getDados(el) {
    div = el.parentElement.parentElement;
    if (div.classList[0] != "acao-elemento"){
        div = div.parentElement;
    }

    var id            = parseInt(div.getAttribute('data-id'));
    var nome          = div.getElementsByClassName('span-nome')[0].innerHTML;
    var valor_atual   = div.getElementsByClassName('span-valor-atual')[0].innerHTML;
    var valor_inicial = div.getElementsByClassName('span-valor-inicial')[0].innerHTML;
    var qnt           = div.getElementsByClassName('span-qnt')[0].innerHTML;
    var change        = div.getElementsByClassName('span-change')[0].innerHTML.split(' ')[0];
    
    res = {
            'id':id,
            'nome':nome, 
            'valor_atual':valor_atual, 
            'valor_inicial':valor_inicial, 
            'qnt':qnt, 
        };
            
    return res;
}

function formFechamentoSubmit(el) {
    // lendo valor de fechamento
    valor_fechamento = el.getElementsByClassName('acao-valor-fechamento')[0].value;

    // lendo id da acao de fechamento
    div = el;
    while(div.classList[0] != "acao-elemento"){
        div = div.parentElement;
    }
    id = parseInt(div.getAttribute('data-id'));

    acaoDB.fechaAcao(id, valor_fechamento, function(){
        console.log('fechei acao ' + id);
        criaAcoes();
    });


    console.log('fechar id : ' + id + ' valor : ' + valor_fechamento);
    return false;
}

function submitFechamento(el) {
    form = el;
    
    // subindo nivel ateh encontrar formulario do input da acao
    while(form.tagName != "FORM"){
        form = form.parentElement;
    }
    
    // executando funcao de fechamento da acao
    formFechamentoSubmit(form)
    return false;
}