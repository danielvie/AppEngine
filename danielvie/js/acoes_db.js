/**
 * Criando indexedDB
 */
var acaoDB = (function() {
 	var aDB = {};
 	var datastore = null;

	// TODO: Add methods for interacting with the database here.
	/**
	* Open a connection to the datastore.
	*/
	aDB.open = function(callback) {
		// Database version.
		var version = 2;

		// Open a connection to the datastore.
		var request = indexedDB.open('acoes', version);

		// Handle datastore upgrades.
		request.onupgradeneeded = function(e) {
			var db = e.target.result;

			e.target.transaction.onerror = aDB.onerror;

			// Delete the old datastore.
			if (db.objectStoreNames.contains('acao')) {
				db.deleteObjectStore('acao');
			}

			// Create a new datastore.
			var store = db.createObjectStore('acao', {
				keyPath: 'timestamp'
			});
		};

		// Handle successful datastore access.
		request.onsuccess = function(e) {
			// Get a reference to the DB.
			datastore = e.target.result;
			console.log('db criado com sucesso.');

			// Execute the callback.
			callback();
		};

		// Handle errors when opening the datastore.
		request.onerror = aDB.onerror;
	};

	/**
	 * Fetch all of the todo items in the datastore.
	 */
	aDB.fetchAcao = function(id,callback) {
	 	var db = datastore;
	 	var transaction = db.transaction(['acao'], 'readonly');
	 	var objStore = transaction.objectStore('acao');

	 	var keyRange = IDBKeyRange.lowerBound(0);
	 	var request = objStore.get(id);
	 	// var request = db.transaction(["estudantes"],"readwrite").objectStore("estudantes").get(codigo);

	 	var acao = [];

	 	transaction.oncomplete = function(e) {
		    // Execute the callback function.
		    callback(acao);
		};

		request.onsuccess = function(e) {
			// request.result.nome
			var result = e.target.result;

			if (!!result == false) {
				return;
			}

		    acao = e.target.result.formulario;
		};

		request.onerror = aDB.onerror;
	};

	/**
	 * Fetch all of the todo items in the datastore.
	 */
	aDB.fetchAcoes = function(callback) {
	 	var db = datastore;
	 	var transaction = db.transaction(['acao'], 'readonly');
	 	var objStore = transaction.objectStore('acao');

	 	var keyRange = IDBKeyRange.lowerBound(0);
	 	var cursorRequest = objStore.openCursor(keyRange);

	 	var acoes = [];

	 	transaction.oncomplete = function(e) {
		    // Execute the callback function.
		    callback(acoes);
		};

		cursorRequest.onsuccess = function(e) {
			var result = e.target.result;

			if (!!result == false) {
				return;
			}

			acoes.push(result.value);

			result.continue();
		};

		cursorRequest.onerror = aDB.onerror;
	};

	/**
	 * Create a new todo item.
	 */
	aDB.createAcao = function(dados, callback) {
		// Get a reference to the db.
		var db = datastore;

		// Initiate a new transaction.
		var transaction = db.transaction(['acao'], 'readwrite');

		// Get the datastore.
		var objStore = transaction.objectStore('acao');

		// Create a timestamp for the todo item.
		var timestamp = new Date().getTime();

		// Create an object for the todo item.
		var acao = {
			'dados': dados,
			'timestamp': timestamp
		};

		// Create the datastore request.
		var request = objStore.put(acao);

		// Handle a successful datastore put.
		request.onsuccess = function(e) {
			// Execute the callback function.
			console.log('acao salva.');
			callback(acao);
		};

		// Handle errors.
		request.onerror = function(e) {
			console.log(aDB.onerror);
		}
	};

	/**
	* Delete a todo item.
	*/
	aDB.deleteAcao = function(id, callback) {
		var db = datastore;
		var transaction = db.transaction(['acao'], 'readwrite');
		var objStore = transaction.objectStore('acao');

		var request = objStore.delete(id);

		request.onsuccess = function(e) {
			callback();
			console.log('acao deletada.');
		};

		request.onerror = function(e) {
			console.log(e);
		};
	};
	
	/**
	* Update a todo item.
	*/
	aDB.updateAcao = function(id, valor_atual, change, callback) {
		var db = datastore;
		var transaction = db.transaction(['acao'], 'readwrite');
		var objStore = transaction.objectStore('acao');

		// var request = objStore.delete(id);

		// lendo valores antigos
		var request = objStore.get(id);

		// var request = store.put(userObject);
    	var status = document.getElementById('acao-status');

		request.onsuccess = function(e) {
			// atualizando valor dos dados
			data = request.result;
			data.dados.valor_atual = valor_atual;
			data.dados.change      = change;

			// requisitando alteracao para BD
			var requestUpdate = objStore.put(data);
			requestUpdate.onerror = function(evt) {
				status.innerHTML = 'houve um erro ao atualizar o valor de ' + data.dados.nome;
				status.className = 'div-ans msg-nok';

			}

			requestUpdate.onsuccess = function(evt) {
				// status.innerHTML = "acao " + data.dados.nome + " atualizada.";
				// status.className = 'div-ans msg-ok';
				// setTimeout(function(){
				// 	status.innerHTML = '';
				// }, 3000);
				
				callback();
			}
		};

		request.onerror = function(e) {
			console.log(e);
		};
	};
	/**
	* Fecha acao
	**/
	aDB.fechaAcao = function(id, valor_fechamento, callback) {
		var db = datastore;
		var transaction = db.transaction(['acao'], 'readwrite');
		var objStore = transaction.objectStore('acao');

		// var request = objStore.delete(id);

		// lendo valores antigos
		var request = objStore.get(id);

		// var request = store.put(userObject);
    	var msg_status = document.getElementById('acao-status');

		request.onsuccess = function(e) {
			// atualizando valor dos dados
			data = request.result;
			data.dados.valor_atual = valor_fechamento;
			data.dados.fechada     = 1;

			// requisitando alteracao para BD
			var requestUpdate = objStore.put(data);
			requestUpdate.onerror = function(evt) {
				msg_status.innerHTML = 'houve um erro ao fechar a acao ' + data.dados.nome;
				msg_status.className = 'div-ans msg-nok';

			}

			requestUpdate.onsuccess = function(evt) {
				msg_status.innerHTML = "acao " + data.dados.nome + " fechada.";
				msg_status.className = 'div-ans msg-ok';
				setTimeout(function(){
					msg_status.innerHTML = '';
				}, 3000);
				
				callback();
			}
		};

		request.onerror = function(e) {
			console.log(e);
		};
	}
	/**
	* Abre acao
	**/
	aDB.abreAcao = function(id, callback) {
		var db = datastore;
		var transaction = db.transaction(['acao'], 'readwrite');
		var objStore = transaction.objectStore('acao');

		// var request = objStore.delete(id);

		// lendo valores antigos
		var request = objStore.get(id);

		// var request = store.put(userObject);
    	var msg_status = document.getElementById('acao-status');

		request.onsuccess = function(e) {
			// atualizando valor dos dados
			data = request.result;
			data.dados.fechada     = 0;

			// requisitando alteracao para BD
			var requestUpdate = objStore.put(data);
			requestUpdate.onerror = function(evt) {
				msg_status.innerHTML = 'houve um erro ao abrir a acao ' + data.dados.nome;
				msg_status.className = 'div-ans msg-nok';

			}

			requestUpdate.onsuccess = function(evt) {
				msg_status.innerHTML = "acao " + data.dados.nome + " aberta.";
				msg_status.className = 'div-ans msg-ok';
				setTimeout(function(){
					msg_status.innerHTML = '';
				}, 3000);
				
				callback();
			}
		};

		request.onerror = function(e) {
			console.log(e);
		};
	}

	// Export the aDB object.
	return aDB;
}());
