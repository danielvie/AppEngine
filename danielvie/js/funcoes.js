Este = 0;
Norte = 0;

function troca(nro) {

	// selecionando elemento que vai ser o novo 1o
	switch(nro){
		case 1: // CONVERSAO LATLON
			el = document.getElementById('div-conversao-latlon');
			break;
		case 2: // CONTA DO ZE
			el = document.getElementById('div-conta-do-ze');
			break;
		case 3: // IMPACTO PO
			el = document.getElementById('div-impacto-po');
			break;
	}

	todos = document.getElementsByClassName('row');
	for (var i = 0; i < todos.length; i++) {
		todos[i].setAttribute('class','row row-escondido');
	}

	el.setAttribute('class','row');

	// salvando elemento escolhido
	aux = el;

	// removendo elemento da pagina atual
	// el.remove();

	// salvando e removendo menu
	// menu = document.getElementById("link-elementos");
	// menu_ = menu;
	// menu.remove();

	// // salvando o restante dos elementos
	// body_html = document.body.innerHTML;

	// // limpando corpo
	// document.body.innerHTML = "";

	// // restaurando elementos
	// document.body.appendChild(menu_);
	// document.body.appendChild(aux);
	// document.body.innerHTML += body_html;

	// remarcando link ativo
	troca_menu_active(nro);
	
	return false;
}

function troca_menu_active(nro) {
	// menu-conversao-latlon
	// menu-conta-do-ze
	// menu-impacto-po

	// lendo elemento de menu
	menu = document.getElementById("link-elementos");

	// selecionando elementos da lista
	c = menu.getElementsByTagName("li");

	// varrendo elementos para reescrever ativo
	for (var i = 0; i < c.length; i++) {
		if(nro == (i+1)){
			c[i].setAttribute("class","active");
		} else {
			c[i].setAttribute("class","");
		}
	}
 }

function exemploImpactoPO() {
	// escrevendo valores dos campos
	document.getElementById('po1_deg').value = 57;
	document.getElementById('po1_min').value = 19;
	document.getElementById('po1_sec').value = 30;

	document.getElementById('po1_dec').value = 55.34;

	document.getElementById('po4_deg').value = 301;
	document.getElementById('po4_min').value = 33;
	document.getElementById('po4_sec').value = 20;

	document.getElementById('po4_dec').value = 298.74;

	return false;
}

function limpaImpactoPO() {
	// escrevendo valores dos campos
	document.getElementById('po1_deg').value = '';
	document.getElementById('po1_min').value = '';
	document.getElementById('po1_sec').value = '';

	document.getElementById('po1_dec').value = '';

	document.getElementById('po4_deg').value = '';
	document.getElementById('po4_min').value = '';
	document.getElementById('po4_sec').value = '';

	document.getElementById('po4_dec').value = '';

	document.getElementById('ans_impacto_po').innerHTML = '';

	return false;
}

function calculaImpactoPO() {
	// DEFININDO CONSTANTES
	d2r = Math.PI/180;
	
	// GERANDO POSICAO DOS PO'S
	x1 = 262672;
	y1 = 8250237;
	x4 = 266354;
	y4 = 8248859;

	// LENDO VALORES DOS CAMPOS
	// po1
	po1_deg = parseFloat(document.getElementById('po1_deg').value);
	if (isNaN(po1_deg))
		po1_deg = 0;
	po1_min = parseFloat(document.getElementById('po1_min').value);
	if (isNaN(po1_min))
		po1_min = 0;
	po1_sec = parseFloat(document.getElementById('po1_sec').value);
	if (isNaN(po1_sec))
		po1_sec = 0;
	po1_dec = parseFloat(document.getElementById('po1_dec').value);
	if (isNaN(po1_dec))
		po1_dec = 0;

	// po4
	po4_deg = parseFloat(document.getElementById('po4_deg').value);
	if (isNaN(po4_deg))
		po4_deg = 0;
	po4_min = parseFloat(document.getElementById('po4_min').value);
	if (isNaN(po4_min))
		po4_min = 0;
	po4_sec = parseFloat(document.getElementById('po4_sec').value);
	if (isNaN(po4_sec))
		po4_sec = 0;
	po4_dec = parseFloat(document.getElementById('po4_dec').value);
	if (isNaN(po4_dec))
		po4_dec = 0;

	// CALCULANDO VALOR DECIMAL DOS ANGULOS
	apo1     = po1_deg + po1_min/60 + po1_sec/3600;
	apo4_    = po4_deg + po4_min/60 + po4_sec/3600;
	apo1_dec = po1_dec;
	apo4_dec = po4_dec;
	
	// ajuste para quando tenho soh um teodolito
	if (apo1 == 0){
		apo1 = po1_dec;
	}
	if (apo4_ == 0){
		apo4_ = po4_dec;
	}
	if (po1_dec == 0){
		po1_dec = apo1;
	}
	if (po4_dec == 0){
		po4_dec = apo4_;
	}

	apo1     = (apo1  + po1_dec)/2;
	apo4_    = (apo4_ + po4_dec)/2;

	// ajustando quadrante po4
	apo4  = 360 - apo4_;

	// convertendo para radianos
	apo1r = apo1*d2r;		
	apo4r = apo4*d2r;
	// CALCULANDO IMPACTO

	// calculando inclinacoes das retas
	m  = (y4 - y1)/(x4 - x1);

    m1 = Math.tan(Math.atan(m) - apo1r);
    m4 = Math.tan(apo4r - Math.atan(-m));
    
    xa = (y4 - y1 + x1*m1 - x4*m4)/(m1-m4);
    ya = y1 + m1*(xa - x1);

    // truncando 1 casa decimal na resposta
	// xa = Math.trunc(xa*10)/10;
	// ya = Math.trunc(ya*10)/10;
	xa = xa.toFixed(1);
	ya = ya.toFixed(1);

	// escrevendo resposta
	ans = document.getElementById('ans_impacto_po');

	txt =       'leste impacto = &nbsp' + xa + ' m<br>';
	txt = txt + 'norte impacto = ' + ya + ' m<br>';

	ans.innerHTML = txt;

	return false;
}

function limpa_textarea() {
	document.getElementById('lat_tot').value = "";
	document.getElementById('resposta_deg').innerHTML = "";
	return false;

}

function limpa_conta_do_ze() {
	document.getElementById('formLMU').reset();
	document.getElementById("azi_conta_do_ze").innerHTML = "";
	return false;
}

function calculaUTM_textarea() {
	txt = document.getElementById("lat_tot").value.split('\n');
	ans = document.getElementById('resposta_deg');

	var la = 0;
	var lo = 0;
	for (var i = 0; i < txt.length; i++) {
		// se encontrar string que pode ser uma coordenada lat/lon, executar conversao LLA->UTM:
		// pos = txt[i].search(/\d{2}((\:)|(\s+))\d{2}((\:)|(\s+))\d{2}(\.\d*)?/g);
		var pla = /[sS]\s*([:=])?\s*\d{2}((\:)|(\s+))\d{2}((\:)|(\s+))\d{2}(\.\d*)?/g
		var plo = /[wW]\s*([:=])?\s*\d{2}((\:)|(\s+))\d{2}((\:)|(\s+))\d{2}(\.\d*)?/g

		
		if (a = pla.exec(txt[i])) {
			lo = 0;

			// pegando coordenadas
			coord = a[0];
			coord = coord.replace(/[Ss]\s*([:=])?\s*/g,'');
			coord = coord.replace(/(\:)|(\s+)/g,':').split(':');
			
			// encontrando grau min seg
			la = parseFloat(coord[0]) + parseFloat(coord[1])/60 + parseFloat(coord[2])/3600;
		}

		if (a = plo.exec(txt[i])) {
			
			// pegando coordenadas
			coord = a[0];
			coord = coord.replace(/[Ww]\s*([:=])?\s*/g,'');
			coord = coord.replace(/(\:)|(\s+)/g,':').split(':');
			
			// encontrando grau min seg
			lo = parseFloat(coord[0]) + parseFloat(coord[1])/60 + parseFloat(coord[2])/3600;
		}

		if ((la != 0) && (lo != 0)){
			var res = deg2utm_modificada(-la,-lo);
			
			txt[i] = txt[i] + "<br>";
			txt[i] = txt[i] + "<br>*E: &nbsp" + res[0].toFixed(1) + " m*";
			txt[i] = txt[i] + "<br>*N: " + res[1].toFixed(1) + " m*";
			txt[i] = txt[i] + "<br>";

			la = 0;
			lo = 0;
		}
	}

	ans.innerHTML = txt.join("<br>");
	return false;
}

function escreve_textarea() {
	txt = '';
	txt = 'Área do chifre 30/05'+'\n\n';
	txt = txt + 'UCF '+'\n';
	txt = txt + 'S 15 30 27.30615'+'\n';
	txt = txt + 'W 47 06 29.15145'+'\n';
	txt = txt + 'H 945/954'+'\n\n';
	txt = txt + 'LMU 3'+'\n';
	txt = txt + 'S: 15 30 34.18125'+'\n';
	txt = txt + 'W: 47 06 30.01156'+'\n';
	txt = txt + 'H : 950/960'+'\n\n';
	txt = txt + 'LMU 2'+'\n';
	txt = txt + 'S: 15 30 34.10405'+'\n';
	txt = txt + 'W: 47 06 30.66754'+'\n';
	txt = txt + 'H: 950/963'+'\n';

	lat = document.getElementById('lat_tot');
	lat.value = txt;

	return false;
}

function preencherALVO_avibras () {
	document.getElementById("targ_E").value  = pos_targ[0][0];
	document.getElementById("targ_N").value  = pos_targ[0][1];
	return false;
}

function deg2utm_modificada(Lat,Lon){
	la=Lat;
	lo=Lon;

	pi = Math.PI;	

	sa = 6378137.000000 ; sb = 6356752.314245;

	e2 = Math.sqrt((sa*sa) - (sb*sb))/sb;
	e2cuadrada = e2*e2;
	c = ( sa*sa ) / sb;
	
	lat = la * ( pi / 180 );
	lon = lo * ( pi / 180 );

	Huso = Math.trunc( ( lo / 6 ) + 31);
	S = ( ( Huso * 6 ) - 183 );
	deltaS = lon - ( S * ( pi / 180 ) );

	a = Math.cos(lat) * Math.sin(deltaS);
	epsilon = 0.5 * Math.log( ( 1 +  a) / ( 1 - a ) );
	nu = Math.atan( Math.tan(lat) / Math.cos(deltaS) ) - lat;
	v = ( c / Math.sqrt( 1 +  e2cuadrada * Math.cos(lat) * Math.cos(lat) ) ) * 0.9996;

	ta = ( e2cuadrada / 2 ) * epsilon * epsilon * Math.cos(lat) * Math.cos(lat);
	a1 = Math.sin( 2 * lat );
	a2 = a1 * Math.cos(lat) * Math.cos(lat);
	j2 = lat + ( a1 / 2 );
	j4 = ( ( 3 * j2 ) + a2 ) / 4;
	j6 = ( ( 5 * j4 ) + ( a2 * Math.pow(Math.cos(lat),2) ) ) / 3;
	alfa = ( 3 / 4 ) * e2cuadrada;
	beta = ( 5 / 3 ) * Math.pow(alfa,2);
	gama = ( 35 / 27 ) * Math.pow(alfa,3);
	Bm = 0.9996 * c * ( lat - alfa * j2 + beta * j4 - gama * j6 );
	xx = epsilon * v * ( 1 + ( ta / 3 ) ) + 500000;
	yy = nu * v * ( 1 + ta ) + Bm;


	if (yy<0)
		yy=9999999+yy;

	Este=xx;
	Norte=yy;
	var resposta = [Math.trunc(Este*10)/10, Math.trunc(Norte*10)/10];
	return resposta;
}

function calculaAziReferencia (argument) {
	lmu_E  = parseFloat(document.getElementById("lmu_E").value);
	lmu_N  = parseFloat(document.getElementById("lmu_N").value);
	ref_E  = parseFloat(document.getElementById("ref_E").value);
	ref_N  = parseFloat(document.getElementById("ref_N").value);
	targ_E = parseFloat(document.getElementById("targ_E").value);
	targ_N = parseFloat(document.getElementById("targ_N").value);
	
	AO            = parseFloat(document.getElementById("AO").value);
	headingMedido = parseFloat(document.getElementById("lmu_heading").value);

	origem = [lmu_E,  lmu_N,  0];
	fim    = [ref_E,  ref_N,  0];
	alvo   = [targ_E, targ_N, 0];
	
	ang_             = conta_do_ze(origem,fim);
	dgt              = conta_do_ze(origem,alvo);
	headingCalculado = (ang_ - AO);
	if (headingCalculado < 0)
		headingCalculado = headingCalculado + 6400;
	
	diff             = headingCalculado - headingMedido;
	
	ang_             = Math.trunc(ang_*100)/100;
	dgt              = Math.trunc(dgt*100)/100;
	diff             = Math.trunc(diff*100)/100;
	headingCalculado = Math.trunc(headingCalculado*100)/100;
	headingMedido    = Math.trunc(headingMedido*100)/100;
	// document.getElementById("azi_conta_do_ze").innerHTML = "ang: " + ang_ + " dgt: " + dgt;

	txt    = "Direcao de Referencia (DR): " + ang_;
	txt    = txt + "<br>" + "Heading (DR - AO): " + headingCalculado; 
	txt    = txt + "<br>" + "diff Heading: " + diff; 
	txt    = txt + "<br>" + "DGT: " + dgt; 
	document.getElementById("azi_conta_do_ze").innerHTML = txt;
	return false;
}

function bradock_produtoEscalar (x,y) {
	ret = (x[0]*y[0]+x[1]*y[1]+x[2]*y[2]);
	return ret;
}

function bradock_norma(x){
// retorna a norma do vetor x[3]
ret = Math.sqrt(bradock_produtoEscalar(x,x));
return ret;
}


function conta_do_ze(origem,fim) {
	v1 = [0, 0, 0];
	v2 = [0, 1, 0];
	
	v1[0] = fim[0] - origem[0];
	v1[1] = fim[1] - origem[1];
	v1[2] = 0;

	// produto escalar para encontrar o angulo
	ang   = Math.acos(bradock_produtoEscalar(v1,v2)/bradock_norma(v1)/bradock_norma(v2));
	// produto vetorial para encontrar o sinal
	ang   = ((v1[1]*v2[0] - v1[0]*v2[1])>1)?(2*3.14159265359-ang):ang;

	// ang = v1[0] + v2[0];
	ret = ang*180/Math.PI/0.05625;
	return ret;
}